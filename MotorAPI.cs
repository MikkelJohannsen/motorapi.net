﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

public class MotorAPI
{
    private HttpClient _httpClient;

    public MotorAPI(string apiKey)
    {
        _httpClient = new HttpClient();
        _httpClient.BaseAddress = new Uri("https://v1.motorapi.dk");
        _httpClient.DefaultRequestHeaders.Add("X-AUTH-TOKEN", apiKey);
    }

    public async Task<Vehicle> FindVehicleByPlateAsync(string plate)
    {
        var response = await _httpClient.GetAsync($"/vehicles/{plate}");
        if (response.IsSuccessStatusCode)
        {
            return await response.Content.ReadAsAsync<Vehicle>();
        }
        return null;
    }
}

public static class HttpExtensions
{
    public static async Task<T> ReadAsAsync<T>(this HttpContent content)
    {
        var json = await content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<T>(json);
    }
}

public class Vehicle
{
    [JsonProperty("registration_number")]
    public string RegistrationNumber { get; set; }
    [JsonProperty("status")]
    public string Status { get; set; }
    [JsonProperty("status_date")]
    public DateTime StatusDate { get; set; }
    [JsonProperty("type")]
    public string Type { get; set; }
    [JsonProperty("use")]
    public string Use { get; set; }
    [JsonProperty("first_registration")]
    public DateTime FirstRegistration { get; set; }
    [JsonProperty("vin")]
    public string Vin { get; set; }
    [JsonProperty("own_weight")]
    public int OwnWeight { get; set; }
    [JsonProperty("total_weight")]
    public int TotalWeight { get; set; }
    [JsonProperty("axels")]
    public int Axles { get; set; }
    [JsonProperty("pulling_axels")]
    public int PullingAxles { get; set; }
    [JsonProperty("seats")]
    public int Seats { get; set; }
    [JsonProperty("coupling")]
    public bool Coupling { get; set; }
    [JsonProperty("doors")]
    public int Doors { get; set; }
    [JsonProperty("make")]
    public string Make { get; set; }
    [JsonProperty("model")]
    public string Model { get; set; }
    [JsonProperty("variant")]
    public string Variant { get; set; }
    [JsonProperty("model_type")]
    public string ModelType { get; set; }
    [JsonProperty("model_year")]
    public int ModelYear { get; set; }
    [JsonProperty("color")]
    public string Color { get; set; }
    [JsonProperty("chassis_type")]
    public string ChassisType { get; set; }
    [JsonProperty("engine_cylinders")]
    public int EngineCylinders { get; set; }
    [JsonProperty("engine_volume")]
    public double EngineVolume { get; set; }
    [JsonProperty("engine_power")]
    public double EnginePower { get; set; }
    [JsonProperty("fuel_type")]
    public string FuelType { get; set; }
    [JsonProperty("registration_zipcode")]
    public string RegistrationZipCode { get; set; }
    [JsonProperty("vehicle_id")]
    public string VehicleId { get; set; }
    [JsonProperty("batch_id")]
    public string BatchId { get; set; }
}